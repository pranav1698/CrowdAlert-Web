const styles = {
  darkBackground: {
    background: '#333333',
  },
  fitContainer: {
    height: '100vh',
  },
  sidebar: {
    width: '75%',
  },
};

export default styles;

import Header from './Header';
import Footer from './Footer';
import Body from './Body';

/**
 * [EventCard export the event card & its associates]
 * @type {Object}
 */
const EventCard = {
  Header,
  Footer,
  Body,
};

export default EventCard;

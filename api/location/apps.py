""" Django based settings
"""

from django.apps import AppConfig


class LocationConfig(AppConfig):
    """ Internal cconfig for django app
    """
    name = 'location'
